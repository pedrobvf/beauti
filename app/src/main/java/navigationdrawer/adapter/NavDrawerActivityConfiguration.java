package navigationdrawer.adapter;

import navigationdrawer.model.NavDrawerItem;
import android.widget.BaseAdapter;

public class NavDrawerActivityConfiguration {

    private int mainLayout;//layout of the activity (the one with the DrawerLayout component)
    private int drawerShadow;//drawable for the shadow of the menu
    private int drawerLayoutId;//id of the DrawerLayout component
    private int leftDrawerId;//id of the component to slide from the left (ListView)
    private int[] actionMenuItemsToHideWhenDrawerOpen;//menu items of the action bar to hide when the drawer is opened.
    private NavDrawerItem[] navItems;//elements of the menu (section/items)
    private int drawerOpenDesc;//description of opened drawer (accessibility)
    private int drawerCloseDesc;//description of closed drawer (accessibility)
    private BaseAdapter baseAdapter;//adapter for the ListView

    public int getMainLayout() {
        return mainLayout;
    }

    public void setMainLayout(int mainLayout) {
        this.mainLayout = mainLayout;
    }

    public int getDrawerShadow() {
        return drawerShadow;
    }

    public void setDrawerShadow(int drawerShadow) {
        this.drawerShadow = drawerShadow;
    }

    public int getDrawerLayoutId() {
        return drawerLayoutId;
    }

    public void setDrawerLayoutId(int drawerLayoutId) {
        this.drawerLayoutId = drawerLayoutId;
    }

    public int getLeftDrawerId() {
        return leftDrawerId;
    }

    public void setLeftDrawerId(int leftDrawerId) {
        this.leftDrawerId = leftDrawerId;
    }

    public int[] getActionMenuItemsToHideWhenDrawerOpen() {
        return actionMenuItemsToHideWhenDrawerOpen;
    }

    public void setActionMenuItemsToHideWhenDrawerOpen(
            int[] actionMenuItemsToHideWhenDrawerOpen) {
        this.actionMenuItemsToHideWhenDrawerOpen = actionMenuItemsToHideWhenDrawerOpen;
    }

    public NavDrawerItem[] getNavItems() {
        return navItems;
    }

    public void setNavItems(NavDrawerItem[] navItems) {
        this.navItems = navItems;
    }

    public int getDrawerOpenDesc() {
        return drawerOpenDesc;
    }

    public void setDrawerOpenDesc(int drawerOpenDesc) {
        this.drawerOpenDesc = drawerOpenDesc;
    }

    public int getDrawerCloseDesc() {
        return drawerCloseDesc;
    }

    public void setDrawerCloseDesc(int drawerCloseDesc) {
        this.drawerCloseDesc = drawerCloseDesc;
    }

    public BaseAdapter getBaseAdapter() {
        return baseAdapter;
    }

    public void setBaseAdapter(BaseAdapter baseAdapter) {
        this.baseAdapter = baseAdapter;
    }
    
}
