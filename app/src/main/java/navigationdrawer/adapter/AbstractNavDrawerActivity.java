package navigationdrawer.adapter;

import com.example.beauti.R;
import navigationdrawer.model.NavDrawerItem;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;


public abstract class AbstractNavDrawerActivity extends FragmentActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    
    private ListView mDrawerList;
    
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    private NavDrawerActivityConfiguration navConf ;
    
    protected abstract NavDrawerActivityConfiguration getNavDrawerConfiguration();
    
    protected abstract void onNavItemSelected( int id );
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        navConf = getNavDrawerConfiguration();
        
        setContentView(navConf.getMainLayout()); 
        
        
        //Configuracao da lista do menu
        mTitle = mDrawerTitle = getTitle();
        
        mDrawerLayout = (DrawerLayout) findViewById(navConf.getDrawerLayoutId());
        mDrawerList = (ListView) findViewById(navConf.getLeftDrawerId());
        mDrawerList.setAdapter(navConf.getBaseAdapter());
        
        //Configura um click em um item do Navigation Drawer
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        this.initDrawerShadow();
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        
        //Configuracao do selector no action bar
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,					/* DrawerLayout object */
                getDrawerIcon(),				/* nav drawer icon to replace 'Up' caret */
                navConf.getDrawerOpenDesc(),	/* "open drawer" description */
                navConf.getDrawerCloseDesc()	/* "close drawer" description */
                ) {
        	
            /* Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
            	//Evento que informara que o drawer foi fechado
                //getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
            
            /* Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
            	//Evento que informara que o drawer foi aberto
                //getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }
    
    protected void initDrawerShadow() {
        mDrawerLayout.setDrawerShadow(navConf.getDrawerShadow(), GravityCompat.START);
    }
    
    protected int getDrawerIcon() {
        return R.drawable.ic_drawer;
    }
    
    /**
     * onPostCreate
     * e usado para fazer tratamentos uma vez que a view ja foi criada para esta activity
     * Neste caso, e usado para actualizar o status do botao da action bar
     **/
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    
    /**
     * onConfigurationChanged
     * e chamada quando a activity ouve uma chamada para alterar as suas configuracoes de estilo/funcionamento
     * Aqui, avisamos o selector do action bar que houve uma mudanca de configuracao
     **/
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	// Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if ( navConf.getActionMenuItemsToHideWhenDrawerOpen() != null ) {
            boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
            for( int iItem : navConf.getActionMenuItemsToHideWhenDrawerOpen()) {
                menu.findItem(iItem).setVisible(!drawerOpen);
            }
        }
        // Handle your other action bar items...
        return super.onPrepareOptionsMenu(menu);
    }
    
    /**
     * onOptionsItemSelected
     * � usado para reconhecer toques nos itens do menu e icones da action bar
     * Para este exemplo, � usado para tratar o toque no �cone da aplica��o. Ser� respons�vel por abrir/fechar o navigation drawer
     **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
                this.mDrawerLayout.closeDrawer(this.mDrawerList);
            }
            else {
                this.mDrawerLayout.openDrawer(this.mDrawerList);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    protected DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    protected ActionBarDrawerToggle getDrawerToggle() {
        return mDrawerToggle;
    }
   
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
    
    public void selectItem(int position) {
        NavDrawerItem selectedItem = navConf.getNavItems()[position];
        
        this.onNavItemSelected(selectedItem.getId());
        mDrawerList.setItemChecked(position, true);
        
        if ( selectedItem.updateActionBarTitle()) {
            setTitle(selectedItem.getLabel());
        }
        
        if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }
    
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
}
