package navigationdrawer.adapter;

import com.example.beauti.R;
import navigationdrawer.model.NavDrawerItem;
import navigationdrawer.model.NavMenuHeader;
import navigationdrawer.model.NavMenuItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/*The adapter to use with the ListView of the menu. It can handle both NavMenuSection and NavMenuItem classes.*/
public class NavDrawerAdapter extends ArrayAdapter<NavDrawerItem> {

    private LayoutInflater inflater;
    
    public NavDrawerAdapter(Context context, int textViewResourceId, NavDrawerItem[] objects ) {
        super(context, textViewResourceId, objects);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null ;
        NavDrawerItem menuItem = this.getItem(position);
        if ( menuItem.getType() == NavMenuItem.ITEM_TYPE ) {
            view = getItemView(position, convertView, parent, menuItem);
        }
        else {
            view = getSectionView(convertView, parent, menuItem);
        }
        return view ;
    }
    
    public View getItemView( int position, View convertView, ViewGroup parentView, NavDrawerItem navDrawerItem ) {
        
        NavMenuItem menuItem = (NavMenuItem) navDrawerItem ;
        NavMenuItemHolder navMenuItemHolder = null;
        
        if (convertView == null) {
            convertView = inflater.inflate( R.layout.drawer_item, parentView, false);
            TextView labelView = (TextView) convertView.findViewById( R.id.navmenuitem_description );
            ImageView iconView = (ImageView) convertView.findViewById( R.id.navmenuitem_icon );
            TextView countView = (TextView) convertView.findViewById( R.id.navmenuitem_counter );

            navMenuItemHolder = new NavMenuItemHolder();
            navMenuItemHolder.labelView = labelView ;
            navMenuItemHolder.iconView = iconView ;
            navMenuItemHolder.countView = countView;

            convertView.setTag(navMenuItemHolder);
        }

        if ( navMenuItemHolder == null ) {
            navMenuItemHolder = (NavMenuItemHolder) convertView.getTag();
        }
                    
        navMenuItemHolder.labelView.setText(menuItem.getLabel());
        navMenuItemHolder.iconView.setImageResource(menuItem.getIcon());
        
        NavDrawerItem item = this.getItem(position);
        
        // displaying count
        // check whether it set visible or not
        if(item.getCounterVisibility())
        {
        	navMenuItemHolder.countView.setText(menuItem.getCount());
        }
        else
        {
        	navMenuItemHolder.countView.setVisibility(View.GONE);
        }

        return convertView ;
    }

    public View getSectionView(View convertView, ViewGroup parentView,
            NavDrawerItem navDrawerItem) {
        
        NavMenuHeader menuSection = (NavMenuHeader) navDrawerItem ;
        NavMenuSectionHolder navMenuItemHolder = null;
        
        if (convertView == null) {
            convertView = inflater.inflate( R.layout.drawer_header, parentView, false);
            TextView labelView = (TextView) convertView.findViewById( R.id.navmenuitem_header);

            navMenuItemHolder = new NavMenuSectionHolder();
            navMenuItemHolder.labelView = labelView ;
            convertView.setTag(navMenuItemHolder);
        }

        if ( navMenuItemHolder == null ) {
            navMenuItemHolder = (NavMenuSectionHolder) convertView.getTag();
        }
                    
        navMenuItemHolder.labelView.setText(menuSection.getLabel());
        
        return convertView ;
    }
    
    @Override
    public int getViewTypeCount() {
        return 2;
    }
    
    @Override
    public int getItemViewType(int position) {
        return this.getItem(position).getType();
    }
    
    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }
    
    
    private static class NavMenuItemHolder {
        public TextView countView;
		private TextView labelView;
        private ImageView iconView;
    }
    
    private class NavMenuSectionHolder {
        private TextView labelView;
    }
}
