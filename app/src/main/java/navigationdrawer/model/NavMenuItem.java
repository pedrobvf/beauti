package navigationdrawer.model;

import android.content.Context;

public class NavMenuItem implements NavDrawerItem {

    public static final int ITEM_TYPE = 1 ;

    private int id ;
    private String label ;  
    private int icon ;
    private boolean updateActionBarTitle ;
    private String count = "0";
	// boolean to set visiblity of the counter
	private boolean isCounterVisible = false;

    private NavMenuItem() {
    }

    public static NavMenuItem create( int id, String label, boolean updateActionBarTitle, Context context) {
        NavMenuItem item = new NavMenuItem();
        item.setId(id);
        item.setLabel(label);
        item.setUpdateActionBarTitle(updateActionBarTitle);
        return item;
    }

    public static NavMenuItem create( int id, String icon, String label, boolean updateActionBarTitle, Context context) {
        NavMenuItem item = new NavMenuItem();
        item.setId(id);
        item.setLabel(label);
        item.setIcon(context.getResources().getIdentifier( icon, "drawable", context.getPackageName()));
        item.setUpdateActionBarTitle(updateActionBarTitle);
        return item;
    }
    
    public static NavMenuItem create( int id, String icon, String label, boolean updateActionBarTitle, Context context, boolean isCounterVisible, String count ) {
        NavMenuItem item = new NavMenuItem();
        item.setId(id);
        item.setLabel(label);
        item.setIcon(context.getResources().getIdentifier( icon, "drawable", context.getPackageName()));
        item.setUpdateActionBarTitle(updateActionBarTitle);
        item.setCounterVisibility(isCounterVisible);
        item.setCount(count);
        return item;
    }
    
    @Override
    public int getType() {
        return ITEM_TYPE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean updateActionBarTitle() {
        return this.updateActionBarTitle;
    }

    public void setUpdateActionBarTitle(boolean updateActionBarTitle) {
        this.updateActionBarTitle = updateActionBarTitle;
    }
    
    /*Contador*/
    public String getCount(){
		return this.count;
	}
    
    public void setCount(String count){
		this.count = count;
	}
    
    public boolean getCounterVisibility(){
		return this.isCounterVisible;
	}
    
    public void setCounterVisibility(boolean isCounterVisible){
		this.isCounterVisible = isCounterVisible;
	}
}