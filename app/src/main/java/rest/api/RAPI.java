package rest.api;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.example.beauti.LoginActivity;

import java.io.BufferedReader;
 
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
 
 
import java.util.List;



/**
 * Created by MiguelQ on 11-05-2014.
 * Rights reserved
 */


 public class  RAPI extends AsyncTask<String,Void,String>  {


    private LoginActivity.FragmentCallback mFragmentCallback;
    private Context act;
    private String method;
    private List<NameValuePair> parameters;
    private Bitmap post_bitmap;
    public static String domain_protocol="http://";
    private static String domain="api.beauti.pt/";
boolean outside_request;

	
//POST varias variaveis
//Overload the method for the 2 cases
    public RAPI(Context ctx, String api_method,List<NameValuePair> p,  LoginActivity.FragmentCallback fragmentCallback) {
        act=ctx;
        method=api_method;
        mFragmentCallback = fragmentCallback;
        parameters=p;
        outside_request=false;
    }


//GET
    //Overload the method for the 2 cases
    public RAPI(Context ctx, String api_method, LoginActivity.FragmentCallback fragmentCallback) {
        act = ctx;
        method = api_method;
        mFragmentCallback = fragmentCallback;
        outside_request=false;
    }

    //EXTERNAL API CALLS ex: facebook graph
    public RAPI(Context ctx, boolean outside, LoginActivity.FragmentCallback fragmentCallback) {
        act = ctx;
        outside_request=outside;
        method="GET";
        mFragmentCallback = fragmentCallback;
    }

    //Post Image Upload case
    //Overload the method for the 2 cases
    public RAPI(Context ctx, String api_method,Bitmap tmp, LoginActivity.FragmentCallback fragmentCallback) {
        act = ctx;
        method = api_method;
        post_bitmap=tmp;
        mFragmentCallback = fragmentCallback;
        outside_request=false;
    }


    @Override
    protected String doInBackground(String... urls) {

        String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
        if (isConnected(act)) {
            String APIurl = "" ;
            if(outside_request){
                APIurl= urls[0];
            }else{
            	//url onde � feito o pedido
            	APIurl= domain_protocol + domain + urls[0];
            }

            if (method.equals("GET")) { return GET(Uri.encode(APIurl, ALLOWED_URI_CHARS)); }
            if (method.equals("POST")) return POST(Uri.encode(APIurl,ALLOWED_URI_CHARS));
 
        } else {
            Log.i("CONN", " CONNECTION FAIL");
            return "error";
        }
        return "null";
    }



    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String results) {
      Log.i("CONN TASK FUNALLE", results);

        if (isConnected(act)) {
            if (mFragmentCallback != null) {
                mFragmentCallback.onTaskDone(results);
            }
        }
    }



    //Verifica se existe conexao
    public boolean isConnected(Context ctx){
        ConnectivityManager connMgr = (ConnectivityManager) ctx.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }




    public static String GET(String url) {
        InputStream inputStream;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            Log.i("HTTP GET:",url);
            // receive response as inputStream
            //inputStream = httpResponse.getEntity().getContent();

            result = EntityUtils.toString(httpResponse.getEntity());

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    public String POST(String url) {
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);


            // Add your data
             List<NameValuePair> EncodedEntities;

             EncodedEntities=parameters;

            Log.d("POST-ARGUMENTS POST", EncodedEntities.toString());

    //        nameValuePairs.add(new BasicNameValuePair("sessionid", "234"));


            //httppost.setEntity(new UrlEncodedFormEntity(EncodedEntities));
            UrlEncodedFormEntity form;
            form = new UrlEncodedFormEntity(EncodedEntities);
            //form.setContentEncoding(HTTP.UTF_8);
            // httppost.setEntity(form);


           httppost.setEntity(new UrlEncodedFormEntity(parameters,"utf-8"));

           // make GET request to the given URL
           HttpResponse httpResponse = httpclient.execute(httppost);


           result = EntityUtils.toString(httpResponse.getEntity(),HTTP.UTF_8);



        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }




}
