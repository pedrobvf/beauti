package listview.classes;

import android.widget.ImageView;

public class AppointmentListViewItem {
	private String name, salon, date, time, content;
    private boolean isButton1Visible = false;
    private boolean isButton2Visible = false;
    private String id;

    private ImageView image;
    private int telefone;
 
    public AppointmentListViewItem(String name, String salon, String date, String content, boolean isButton1Visible, boolean isButton2Visible) {
    	this.name = name;
        this.salon = salon;
        this.date = date;
        this.content = content;
        this.setButton1Visibility(isButton1Visible);
        this.setButton2Visibility(isButton2Visible);
    }

    public AppointmentListViewItem(String id, String name, String salon, String date, String content, boolean isButton1Visible, boolean isButton2Visible) {
        this.id = id;
        this.name = name;
        this.salon = salon;
        this.date = date;
        this.content = content;
        this.setButton1Visibility(isButton1Visible);
        this.setButton2Visibility(isButton2Visible);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getSalon() {
        return salon;
    }
    
    public void setSalon(String salon) {
        this.salon = salon;
    }
    
    public String getTime() {
        return time;
    }
    
    public void setTime(String time) {
        this.time = time;
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    /*visibilidade dos botoes*/
    public boolean getButton1Visibility(){
		return this.isButton1Visible;
	}
    
    public void setButton1Visibility(boolean isButton1Visible){
		this.isButton1Visible = isButton1Visible;
	}
    
    public boolean getButton2Visibility(){
		return this.isButton2Visible;
	}
    
    public void setButton2Visibility(boolean isButton2Visible){
		this.isButton2Visible = isButton2Visible;
	}


    public int getTel() {
        return telefone;
    }

    public void setTel(int tel) {
        this.telefone = tel;
    }


}
