package listview.classes;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beauti.LoginActivity;
import com.example.beauti.MarcacaoEfetuadaFragment;
import com.example.beauti.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rest.api.RAPI;

public class ProfissionalCustomListViewAdapter extends ArrayAdapter<ProfissionalListViewItem> {

    private final Context context;
    private final ArrayList<ProfissionalListViewItem> itemsArrayList;

    String tokenSaved, contacto, idServico, profissionalID, data_hora;

    private int yearStr, monthStr, dayStr;

    // key and value pair
    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

    public ProfissionalCustomListViewAdapter(Context context, ArrayList<ProfissionalListViewItem> itemsArrayList) {
        super(context, R.layout.listview, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;

        // Usa o SharedPreferences para  buscar o token
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        tokenSaved = preferences.getString("token", "");
        contacto =  preferences.getString("user_phone", "");
        idServico =  preferences.getString("id_servico", "");
        profissionalID = preferences.getString("agente_id", "");
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // 1. Create inflater
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.profissional_listitem_layout, parent, false);

        // 3. Get the tree text view from the rowView
        TextView servico = (TextView) rowView.findViewById(R.id.servico_efetuado);
        TextView preco = (TextView) rowView.findViewById(R.id.tempo_demora);

        // 4. Set the text for textView
        servico.setText(itemsArrayList.get(position).getServico());
        preco.setText(itemsArrayList.get(position).getValorPreco());


        Button botao = (Button) rowView.findViewById(R.id.reserva);
        botao.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String id_servico = itemsArrayList.get(position).getServicoId();
                String nome_servico = itemsArrayList.get(position).getServico();
                String valor_preco = itemsArrayList.get(position).getValorPreco();

                savePreferences("servico", nome_servico);
                savePreferences("valor_preco", valor_preco);
                savePreferences("id_servico", id_servico);


                datePickerDialog();
            }
        });


        // 5. return rowView
        return rowView;
    }


    /**------------------------------------------------------
     ** 		    Mostra o calendário. 					*
     **------------------------------------------------------*/
    /*Date Dialog*/
    private void datePickerDialog(){

        final Calendar c = Calendar.getInstance();
        yearStr = c.get(Calendar.YEAR);
        monthStr = c.get(Calendar.MONTH);
        dayStr = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                // Do something with the date chosen by the user
                yearStr = selectedYear;
                monthStr = selectedMonth + 1;
                dayStr = selectedDay;


                data_hora = dayStr+"-"+monthStr+"-"+yearStr;


                singleChoiseDialog();
            }
        };
        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, datePickerListener, yearStr, monthStr, dayStr);


        datePickerDialog.show();
    }

    /**------------------------------------------------------
     ** 		    Mostra a hora. 					*
     **------------------------------------------------------*/
    final CharSequence[] horas_escolhidas = {"09:00", "11:00", "14:00", "15:00", "16:00", "17:00", "18:00"};

    //final CharSequence[] horas_escolhidas = horas.toArray(new CharSequence[horas.size()]);

    private void singleChoiseDialog(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Hora");

        //coloca a 1 opcao do array escolhida
        alertDialog.setSingleChoiceItems(horas_escolhidas, -1,
                new DialogInterface.OnClickListener() {

                    //In this function I am passing items array to populate CheckBoxes.
                    //then I am passing a Boolean array with the value selected at true.
                    public void onClick(DialogInterface dialogInterface, int item) {
                        // The 'item' argument contains the index position of the selected item

                    }

                });

        //.setMessage("Are you sure you want to delete this entry?")
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
	        	/* User clicked Yes so do some stuff */
                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();

                //Toast.makeText(getActivity().getApplication(), "Select "+horas_escolhidas[selectedPosition], Toast.LENGTH_SHORT).show();

                /** Add padding to numbers less than ten */
                data_hora = data_hora + " " + horas_escolhidas[selectedPosition];

                //Toast.makeText(getActivity().getApplication(), data_hora, Toast.LENGTH_SHORT).show();

                //Atualiza visualmente
                updateMarcacao();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                });

        alertDialog.show();// create and show the alert dialog
    }


    /**
     * Método que permite atualizar a lista de valores que o POST vai receber
     * */
    public void updateMarcacao(){
        // Building post parameters, key and value pair
        nameValuePair.add(new BasicNameValuePair("token", tokenSaved));
        nameValuePair.add(new BasicNameValuePair("idmarcacao", ""));
        nameValuePair.add(new BasicNameValuePair("date",  data_hora));
        nameValuePair.add(new BasicNameValuePair("contacto", contacto ));
        nameValuePair.add(new BasicNameValuePair("mensagem", "" ));
        nameValuePair.add(new BasicNameValuePair("idprofessional", profissionalID ));
        nameValuePair.add(new BasicNameValuePair("total_servicos", "1" ));
        nameValuePair.add(new BasicNameValuePair("servico_0", idServico ));


        //Guarda dos dados
        efetuar_marcacao();
    }


    /**
     * HTTP Post
     * */
    private void efetuar_marcacao() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);
                    // Getting JSON Object node

                    String marcacao = result.getString("doMarcacao");

                    if(marcacao.equals("1")){
                        Toast.makeText(context,"Marcação realizada com sucesso", Toast.LENGTH_LONG).show();

                        /*Altera de um fragmento para outro*/
                        FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.content_frame, new MarcacaoEfetuadaFragment()).commit();

                    }else{
                        Toast.makeText(context,"A marcação não foi realizada ", Toast.LENGTH_LONG).show();
                    }


                }catch(JSONException e){
                    //Problema no parse
                    e.printStackTrace();
                    Toast.makeText(context,"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        };
        new RAPI(context, "POST", nameValuePair, FC).execute( "/1/marcacoes/doMarcacao.json" );
    }



    /**------------------------------------------------------
     ** 					Save Preferences. 					*
     **------------------------------------------------------*/
    public void savePreferences(String key, String value) {

        //SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); // for private mode

        //Sharedpreferences object
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        //Permite editar Sharedpreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);// Storing string value
        editor.commit();// commit changes
    }

}
