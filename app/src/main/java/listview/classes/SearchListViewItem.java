package listview.classes;


public class SearchListViewItem {

    //@SerializedName("name")
    private String name;
    //private String address;
    private String phone;
    private String image_url;
    private String agenteId;

    /*public SearchListViewItem(String name){
        this.name = name;
    }*/

    public SearchListViewItem(String agenteId, String image_url, String name, String tel){
        this.agenteId = agenteId;
        this.image_url=image_url;
        this.name = name;
        this.phone=tel;
    }

    /*public SearchListViewItem(String agenteId, String image_url, String name, String add, String tel){
        this.agenteId = agenteId;
        this.image_url=image_url;
        this.name = name;
        this.address = add;
        this.phone=tel;
    }*/

    public String getAgenteId() {
        return agenteId;
    }

    public void setAgenteId(String agenteId) {
        this.agenteId = agenteId;
    }

    public String getImage_url(){
        return image_url;
    }

    public void setImage_url(String image_url){
        this.image_url = image_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    /*public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }*/


    public String getPhone() {
        return phone;
    }

    public void setPhone(String tel) {
        this.phone = tel;
    }


}
