package listview.classes;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beauti.ProfissionalFragment;
import com.example.beauti.R;
import com.example.beauti.SearchFragment;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import image.ImageLoader;

public class SearchCustomListViewAdapter extends BaseAdapter {


	private Context context;
    //private List<SearchListViewItem> searchlist = null;

    private Button button;

    private String image, nome, telefone;


    private ArrayList<SearchListViewItem> itemsList;

    ImageLoader imageLoader;

	public SearchCustomListViewAdapter(Context context, ArrayList<SearchListViewItem> searchlist) {
        this.imageLoader=new ImageLoader(context);
		this.context = context;
        this.itemsList = searchlist;
	}

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


            // 1. Create inflater
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            // 2. Get rowView from inflater
        View view = inflater.inflate(R.layout.lista_pesquisa_layout, parent, false);


        //image = itemsList.get(position).getImage();//undefined.png
        nome = itemsList.get(position).getName();
        telefone = itemsList.get(position).getPhone();

        //Get Image
        ImageView imagem = (ImageView)view.findViewById(R.id.thumbnail);


        //new ImageDownloader(image, imagem).execute();
        imageLoader.DisplayImage(itemsList.get(position).getImage_url(), imagem);


         // 3. Get the tree text view from the rowView
        TextView title = (TextView) view.findViewById(R.id.title);
        //TextView address = (TextView) view.findViewById(R.id.morada);
        TextView phone = (TextView) view.findViewById(R.id.telefone);

        // Set the results into TextViews
        title.setText(nome);
        phone.setText(telefone);

        //
        button = (Button) view.findViewById(R.id.marcacao);

        // because, we set the click listener on the button only
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //Toast.makeText(context, searchlist.get(position).getName() + " " + searchlist.get(position).getAddress() + " " + searchlist.get(position).getPhone(), Toast.LENGTH_LONG).show();

                //Vai buscar o id do determinado agente da lista
                String agenteId = itemsList.get(position).getAgenteId();

                //Toast.makeText(context,agenteId, Toast.LENGTH_LONG).show();


                savePreferences("IDAgente", agenteId);

                /*Altera de um fragmento para outro*/
                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.content_frame, new ProfissionalFragment()).commit();
                //transaction.replace(R.id.content_frame, new ProfissionalFragment()).commit();
            }
        });

         
        // 5. return rowView
        return view;
	}

    /**------------------------------------------------------*/
    /* 					Save Preferences. 					*/
    /*------------------------------------------------------*/
    public void  savePreferences(String key, String value) {

        //Sharedpreferences object
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        //Permite editar Sharedpreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);// Storing string value
        editor.commit();// commit changes
    }

    /**
     * Vai buscar à internet as imagens
     * */
    /*private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String url;
        private ImageView imageView;

        public ImageDownloader(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setConnectTimeout(30000);
                connection.setReadTimeout(30000);
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(result);
        }

    }


    // Sort Person list by name ascending
    public void sortByNameAsc() {
        Comparator<SearchListViewItem> comparator = new Comparator<SearchListViewItem>() {

            @Override
            public int compare(SearchListViewItem object1, SearchListViewItem object2) {
                return object1.getName().compareTo(object2.getName());
            }

        };
        Collections.sort(itemsList, comparator);
        notifyDataSetChanged();
    }
    */


}



