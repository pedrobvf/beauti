package listview.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beauti.AppointmentFragment;
import com.example.beauti.LoginActivity;
import com.example.beauti.R;

import org.json.JSONException;
import org.json.JSONObject;

import rest.api.RAPI;

public class AppointmentCustomListViewAdapter extends ArrayAdapter<AppointmentListViewItem> implements OnClickListener {

	private final Context context;
	private final ArrayList<AppointmentListViewItem> itemsArrayList;
	
	private RelativeLayout layout1, layout2;
	private Button button1, button2;

    private String estado;
    String id_appointment;


    private String tokenSaved;

	public AppointmentCustomListViewAdapter(Context context, ArrayList<AppointmentListViewItem> itemsArrayList) {
		super(context, R.layout.listview, itemsArrayList);

		this.context = context;
		this.itemsArrayList = itemsArrayList;

        // Usa o SharedPreferences para  buscar o token
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        tokenSaved = preferences.getString("token", "");

	}
	
	 public View getView(final int position, View convertView, ViewGroup parent) {
		  
		 // 1. Create inflater 
         LayoutInflater inflater = LayoutInflater.from(parent.getContext());
         
         // 2. Get rowView from inflater
         View rowView = inflater.inflate(R.layout.appointment_layout, parent, false);

         //itemsArrayList.get(position).compareTo();
         
         // 3. Get the tree text view from the rowView
         TextView name = (TextView) rowView.findViewById(R.id.name_appointment);
         TextView salon = (TextView) rowView.findViewById(R.id.salon_appointment);
         TextView date = (TextView) rowView.findViewById(R.id.date_appointment);
         TextView time = (TextView) rowView.findViewById(R.id.time_appointment);
         TextView content = (TextView) rowView.findViewById(R.id.text_appointment);
         
         // 4. Set the text for textView
         name.setText(itemsArrayList.get(position).getName());
         salon.setText(itemsArrayList.get(position).getSalon());
         date.setText(itemsArrayList.get(position).getTime());
         time.setText(itemsArrayList.get(position).getDate());
         content.setText(itemsArrayList.get(position).getContent());
         
         
         /* 5. Set visibility of buttons (visible)*/
         //notification_button=(LinearLayout) rootView.findViewById(R.id.notification_button);
         layout1 = (RelativeLayout) rowView.findViewById(R.id.buttons1);
         layout2 = (RelativeLayout) rowView.findViewById(R.id.buttons2);
         
         if(itemsArrayList.get(position).getButton1Visibility())
         {
             layout1.setVisibility(View.VISIBLE);
             layout1.setOnClickListener(this);
         }
         
         if(itemsArrayList.get(position).getButton2Visibility())
         {
             layout2.setVisibility(View.VISIBLE);
             layout2.setOnClickListener(this);
         }

         //6. Click event
         button1 = (Button) rowView.findViewById(R.id.cancel_button2);
         button1.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 //2= cancel
                 estado="8";
                 Toast.makeText(context, "Marcacao Cancelada ", Toast.LENGTH_LONG).show();
                 //Toast.makeText(context, "Marcacao Cancelada " + itemsArrayList.get(position).getId(), Toast.LENGTH_LONG).show();

                 id_appointment = itemsArrayList.get(position).getId();

                 alterar_dados();


                 //Altera de um fragmento para o outro através do FragmentManager, usando o FragmentActivity
                 FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                 FragmentTransaction transaction = fragmentManager.beginTransaction();
                 transaction.replace(R.id.content_frame, new AppointmentFragment()).commit();
             }
         });


         button2 = (Button) rowView.findViewById(R.id.cancel_button1);
         button2.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 estado="8";
                 Toast.makeText(context, "Marcacao Cancelada ", Toast.LENGTH_LONG).show();
                 //Toast.makeText(context, "Marcacao Cancelada " + itemsArrayList.get(position).getId(), Toast.LENGTH_LONG).show();

                 id_appointment = itemsArrayList.get(position).getId();

                 alterar_dados();

                 //Altera de um fragmento para o outro através do FragmentManager, usando o FragmentActivity
                 FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                 FragmentTransaction transaction = fragmentManager.beginTransaction();
                 transaction.replace(R.id.content_frame, new AppointmentFragment()).commit();
             }
         });

         // 5. return rowView
         return rowView;
	}

    /*HTTP Post*/
    private void alterar_dados() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);
                    // Getting JSON Object node


                }catch(JSONException e){
                    //Problema no parse
                    e.printStackTrace();
                    Toast.makeText(context,"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        };
        //http://api.beauti.pt/1/marcacoes/setEstadoMarcacao.json?token=bf86543c0da416447df8f51d1fba6ea9559c2ee6b14586.12883924&id=3732&%20status=8
        new RAPI(context, "GET", FC).execute("1/marcacoes/setEstadoMarcacao.json?token="+tokenSaved+"&id="+id_appointment+"&%20status="+estado);
    }

    @Override
    public void onClick(View v) {

    }


    Date date1, date2;

    /** Sort Appointment list by name ascending */
    public void sortByDateAsc() {

        //Serve para converter a data de String para a date
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Comparator<AppointmentListViewItem> comparator = new Comparator<AppointmentListViewItem>() {

            @Override
            public int compare(AppointmentListViewItem object1, AppointmentListViewItem object2) {

                try {

                    date1 = formatter.parse(object1.getDate());
                    //System.out.println(date1);
                    //System.out.println(formatter.format(date1));

                    date2 = formatter.parse(object2.getDate());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //return object1.getDate().compareTo(object2.getDate());

                return date2.compareTo(date1);
            }

        };
        Collections.sort(itemsArrayList, comparator);
        notifyDataSetChanged();
    }
}
