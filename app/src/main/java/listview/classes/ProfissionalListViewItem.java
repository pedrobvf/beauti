package listview.classes;


public class ProfissionalListViewItem {
    private String servico, valorPreco;
    private String servicoId;

    public ProfissionalListViewItem(String servico, String valorPreco) {
        this.servico = servico;
        this.valorPreco = valorPreco;
    }

    public ProfissionalListViewItem(String servicoId, String servico, String valorPreco) {
        this.servicoId = servicoId;
        this.servico = servico;
        this.valorPreco = valorPreco;
    }

    public String getServicoId(){
        return servicoId;
    }

    public void setServico(String servico) {
        this.servico = servico;
    }

    public String getServico() {
        return servico;
    }

    public void setAgenteId(String servico) {
        this.servico = servico;
    }

    public String getValorPreco(){
        return valorPreco;
    }

    public void setValorPreco(String valorPreco){
        this.valorPreco = valorPreco;
    }

}
