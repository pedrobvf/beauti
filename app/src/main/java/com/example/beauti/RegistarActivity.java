package com.example.beauti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rest.api.RAPI;


public class RegistarActivity extends Activity implements View.OnClickListener{

    EditText pname, lname, enderecoemail, pass;

    // key and value pair
    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registo);


        pname = (EditText) findViewById(R.id.pnome);
        lname = (EditText) findViewById(R.id.lnome);
        enderecoemail = (EditText) findViewById(R.id.enderecoemail);
        pass = (EditText) findViewById(R.id.pass);

        Button registo = (Button) findViewById(R.id.registo);
        Button login = (Button) findViewById(R.id.login_button);

        registo.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id==R.id.registo){

            if (pname.getText().toString().isEmpty() || lname.getText().toString().isEmpty() || enderecoemail.getText().toString().isEmpty() || pass.getText().toString().isEmpty()) {
                Toast.makeText(this.getApplication(), "Por favor preenha os campos!", Toast.LENGTH_LONG).show();
            }
            else {
                //updateRegisto();

                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }

        if(id==R.id.login_button){

            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }


    public void updateRegisto(){
        nameValuePair.add(new BasicNameValuePair("firstname", pname.getText().toString() ));
        nameValuePair.add(new BasicNameValuePair("lastname", lname.getText().toString() ));
        nameValuePair.add(new BasicNameValuePair("username", enderecoemail.getText().toString() ));
        nameValuePair.add(new BasicNameValuePair("password", pass.getText().toString() ));

        registar();
    }

    private void registar() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try {
                    JSONObject result = new JSONObject(results);
                }catch(JSONException e){
                    //Problema no parse
                    e.printStackTrace();
                    Toast.makeText(getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        };
        new RAPI(getApplication(), "POST", nameValuePair, FC).execute( "/1/Users/registerUser.json" );
    }
}
