package com.example.beauti;


import image.ImageLoader;
import rest.api.RAPI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ConfigurationsFragment extends Fragment implements OnClickListener{
	
	Button photo;
	LinearLayout pname_button, lname_button, phone_button;
    public SharedPreferences preferences;
	ImageView fotoPerfil;
	TextView pNome, uNome, email, telefone;
    private String tokenSaved;

    //User
    private String id_user, priNome, ultNome, codPost1, codPost2, user_email, telUtilizador;
    String imagem;

    ProgressBar barraProgresso;


    private static final int SELECTED_PICTURE = 1;
    String imgDecodableString;


    // key and value pair
    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);


    ImageLoader imageLoader;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Alterar o action bar title
        getActivity().getActionBar().setTitle("Definicoes");

        // Usa o SharedPreferences para  buscar os valores
	    preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
	    tokenSaved = preferences.getString("token", "");

        //USER
        id_user = preferences.getString("id_utiliz", "");
        priNome = preferences.getString("pri_nome", "");
        ultNome = preferences.getString("ult_nome", "");
        telUtilizador = preferences.getString("telefone", "");

        //cliente
        codPost1 = preferences.getString("cod_postal1", "");
        codPost2 = preferences.getString("cod_postal2", "");
        user_email = preferences.getString("email", "");

        //Foto
        imagem = preferences.getString("foto", "");

        //Esconde o teclado
        //hideSoftKeyboard();

        //Inicializa o objecto através do método
        this.imageLoader=new ImageLoader(getActivity().getApplication());
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.configurations_layout, container, false);


        //Barra de progresso
        barraProgresso = (ProgressBar) rootView.findViewById(R.id.progressconf);
        
        /*Botoes*/
        photo = (Button) rootView.findViewById(R.id.button);
        photo.setOnClickListener(this);
        
        /*LinearLayouts a se comportar como um Botao*/
        pname_button=(LinearLayout) rootView.findViewById(R.id.pname_button);
        pname_button.setOnClickListener(this);

        lname_button=(LinearLayout) rootView.findViewById(R.id.lname_button);
        lname_button.setOnClickListener(this);
        
        phone_button=(LinearLayout) rootView.findViewById(R.id.phone_button);
        phone_button.setOnClickListener(this);


        /*Find the Editable Views by id*/
        fotoPerfil = (ImageView) rootView.findViewById(R.id.image);

        pNome = (TextView) rootView.findViewById(R.id.pNameView);
        uNome = (TextView) rootView.findViewById(R.id.lNameView);
        email = (TextView) rootView.findViewById(R.id.emailView);
        telefone = (TextView) rootView.findViewById(R.id.phoneView);

        //Faz um request ao diretorio da foto
        //ver_foto();

        imageLoader.DisplayImage(imagem, fotoPerfil);
        barraProgresso.setVisibility(View.GONE);
        //new ImageDownloader(imagem, fotoPerfil).execute();

        
        //Set texts
        pNome.setText(priNome);
    	uNome.setText(ultNome);
        email.setText(user_email);
        telefone.setText(telUtilizador);


        return rootView;
    }

    /*------------------------------------------------------*/
    /* 					Save Preferences. 					*/
    /*------------------------------------------------------*/
    public void  savePreferences(String key, String value) {

        //SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); // for private mode

        //Sharedpreferences object
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Permite editar Sharedpreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);// Storing string value
        editor.commit();// commit changes
    }


    /**------------------------------------------------
     * Método que permite atualizar a lista de valores que o POST vai receber
     **------------------------------------------------*/
    public void updateInfoProfissional(){
        // Building post parameters, key and value pair
        //List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
        nameValuePair.add(new BasicNameValuePair("id", id_user ));
        nameValuePair.add(new BasicNameValuePair("token", tokenSaved));

        nameValuePair.add(new BasicNameValuePair("fname1", pNome.getText() + "" ));
        nameValuePair.add(new BasicNameValuePair("fname2", uNome.getText() + "" ));
        nameValuePair.add(new BasicNameValuePair("fcodp1", codPost1));
        nameValuePair.add(new BasicNameValuePair("fcodp2", codPost2));
        nameValuePair.add(new BasicNameValuePair("telemovel2", telefone.getText() + ""));
    }



	/**
     * Metodo que permite ir buscar fotos atraves de um url
     * */
	/*private class ImageDownloader extends AsyncTask <String, Void, Bitmap>{

		private String url;
		private ImageView imageView;


		  public ImageDownloader(String url, ImageView imageView) {
			  this.url = url;
		      this.imageView = imageView;
		  }

		@Override
		protected Bitmap doInBackground(String... urls) {
            // Download Images from the Internet
			try {
				URL urlConnection = new URL(url);
	            HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setConnectTimeout(30000);
                connection.setReadTimeout(30000);
                connection.setDoInput(true);
	            connection.connect();
	            InputStream input = connection.getInputStream();
	            Bitmap myBitmap = BitmapFactory.decodeStream(input);
	            return myBitmap;
		      } catch (Exception e) {
		    	  e.printStackTrace();
		    	  return null;
		      }
		}

		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
            // If this item hasn't been recycled already, hide the
            // progress and set and show the image
            barraProgresso.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
			imageView.setImageBitmap(result);
		}

	}*/
	
	@Override
	public void onClick(View v) {

		int id = v.getId();

		if(id==R.id.button){
            // Create intent to Open Image applications like Gallery, Google Photos
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            startActivityForResult(galleryIntent, SELECTED_PICTURE);

		}
        else if(id==R.id.pname_button){
			//Toast.makeText(this.getActivity(), "Editar Primeiro Nome!", Toast.LENGTH_LONG).show();
			//alertDialog("Ultimo Nome", "Ricardo");
			alertDialog("Primeiro Nome", pNome, id);
		}
        else if(id==R.id.lname_button){
			//Toast.makeText(this.getActivity(), "Editar Email!", Toast.LENGTH_LONG).show();
			//alertDialog("Email", "ricardo.boucas@gmail.com");
			alertDialog("Último Nome", uNome, id);
		}
        else if(id==R.id.phone_button){
//			//Toast.makeText(this.getActivity(), "Editar Telefone!", Toast.LENGTH_LONG).show();
			alertDialog("Telemóvel", telefone, id); //limitar o numero de telefone a 14 digitos
		}
	}

    //Once after User picks image, we need to get the picked image data and set it in ImageView.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case SELECTED_PICTURE:
                // When an Image is picked
                if (resultCode == Activity.RESULT_OK){
                    // Get the Image from data

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    // Get the cursor
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    //ImageView imgView = (ImageView) findViewById(R.id.imgView);
                    // Set the Image in ImageView after decoding the String
                    //imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                    fotoPerfil.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

                }else {
                    Toast.makeText(getActivity().getApplication(), "Não Escolheu uma Imagem", Toast.LENGTH_LONG).show();
                }
                break;
            default:
        }

    }


    /**
     * Metodo que criar um AlertDialog
     * */
	private void alertDialog(String title, final TextView nome, final int id){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//		//alertDialog.setTitle("First Name");
		alertDialog.setTitle(title);

        //Get the text out of a TextView or EditText.
		String texto= nome.getText().toString();

        // Set an EditText view to get user input
		final EditText input = new EditText(getActivity());
		input.setHint("Nome");

		input.setText(texto);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {

//	        	/* User clicked Yes so do some stuff */

//	        	//Get input value
	        	String value = input.getText().toString();
//
	        	//Change What's inside textView of pNome
	        	nome.setText(value);

                ///Ao clicar no botao respetivo atualiza a info respetiva
                if(id==R.id.pname_button) {
                    //Guarda nas preferências o nome
                    savePreferences("pri_nome", value);

                    //Atualiza visualmente
                    updateInfoProfissional();

                    //Guarda dos dados
                    alterar_dados();

                }
                else if(id==R.id.lname_button) {
                    //Guarda nas preferências o nome
                    savePreferences("ult_nome", value);

                    //Atualiza visualmente
                    updateInfoProfissional();

                    //Guarda dos dados
                    alterar_dados();
                }
                else if(id==R.id.phone_button){
                    //Guarda nas preferências o nome
                    savePreferences("telefone", value);

                    //Atualiza visualmente
                    updateInfoProfissional();

                    //Guarda dos dados
                    alterar_dados();
                }

	        }
	     })
	    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	/* User clicked No so do some stuff */
	        }
	     })
	    //.setIcon(android.R.drawable.ic_dialog_alert)
	    .show();// create and show the alert dialog
    }


    /**
     * HTTP Post
     * */
    private void alterar_dados() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);
                    // Getting JSON Object node


                }catch(JSONException e){
                    //Problema no parse
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplication(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        };
        new RAPI(getActivity().getApplication(), "POST", nameValuePair, FC).execute( "1/Users/editUser.json" );
    }



    /**
     * Esconde o teclado
     * */
    /*public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(getActivity().getApplication().INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }*/

}
