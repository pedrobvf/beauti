package com.example.beauti;

//import rest.api.RAPI;
import navigationdrawer.adapter.AbstractNavDrawerActivity;
import navigationdrawer.adapter.NavDrawerActivityConfiguration;
import navigationdrawer.adapter.NavDrawerAdapter;
import navigationdrawer.model.NavDrawerItem;
import navigationdrawer.model.NavMenuHeader;
import navigationdrawer.model.NavMenuItem;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


public class MainActivity extends AbstractNavDrawerActivity {

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ( savedInstanceState == null ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SearchFragment()).commit();
        }

    }


    @Override
    protected NavDrawerActivityConfiguration getNavDrawerConfiguration() {

        
        NavDrawerItem[] menu = new NavDrawerItem[] {
        	NavMenuItem.create(0, "Perfil", false, this),
	        NavMenuHeader.create(1, "PESQUISA"),
	        NavMenuItem.create(2, "ic_search", "Efetuar Pesquisa", true, this),
	        NavMenuHeader.create(3, "AREA PESSOAL"),
            NavMenuItem.create(4, "ic_paste", "Ver Marcacoes Agendadas", true, this),
	        NavMenuHeader.create(5, "COMUNIDADE"),
	        NavMenuItem.create(6, "ic_facebook", "Gostar", false, this),
	        NavMenuItem.create(7, "ic_printest", "Seguir", false, this),
	        NavMenuHeader.create(8, "OUTROS"),
	        //NavMenuItem.create(9, "ic_new_email", "Apoio ao Cliente", true, this),
	        NavMenuItem.create(9, "ic_pc", "Website", false, this),
	        NavMenuItem.create(10, "ic_exit", "Sair", false, this)
        };
        
        NavDrawerActivityConfiguration navDrawerActivityConfiguration = new NavDrawerActivityConfiguration();
        navDrawerActivityConfiguration.setMainLayout(R.layout.main_layout);
        navDrawerActivityConfiguration.setDrawerLayoutId(R.id.drawer_layout);
        navDrawerActivityConfiguration.setLeftDrawerId(R.id.left_drawer);
        navDrawerActivityConfiguration.setNavItems(menu);
        navDrawerActivityConfiguration.setDrawerShadow(R.drawable.drawer_shadow);       
        navDrawerActivityConfiguration.setDrawerOpenDesc(R.string.drawer_open);
        navDrawerActivityConfiguration.setDrawerCloseDesc(R.string.drawer_close);
        navDrawerActivityConfiguration.setBaseAdapter(
            new NavDrawerAdapter(this, R.layout.drawer_item, menu ));
        return navDrawerActivityConfiguration;
    }
    
    @Override
    protected void onNavItemSelected(int id) {
    	switch ((int)id) {
            case 0:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new ConfigurationsFragment()).commit();
                break;
            case 2:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SearchFragment()).commit();
                break;
            case 4:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new AppointmentFragment()).commit();
                break;
            case 6:
                /*Go to facebook Browser*/
                Intent facebookBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/beauti.pt"));
                startActivity(facebookBrowserIntent);
                break;
            case 7:
                /*Go to pinterest Browser*/
                Intent instagramBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://instagram.com/beauti_pt/"));
                startActivity(instagramBrowserIntent);
                break;
            /*case 9:
                //Go to email fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new EmailFragment()).commit();
                break;*/
            case 9:
                /*Go to Browser*/
                Intent beautiIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.beauti.pt"));
                startActivity(beautiIntent);
                break;
            case 10:
                //fecha a aplicacao
                this.finish();
                break;
        }
    	
    	
    }

}
