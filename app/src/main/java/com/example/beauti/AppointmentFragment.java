package com.example.beauti;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import listview.classes.AppointmentListViewItem;
import rest.api.RAPI;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import listview.classes.AppointmentCustomListViewAdapter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class AppointmentFragment extends ListFragment {
	
	//Actividade
	public interface FragmentCallback { public void onTaskDone(String results); }
	
	//A ProgressDialog object
    ProgressBar barraProgresso;


    TextView sem_marcacoes;
    
	
	/*----------------------------------*/
	
	private String tokenSaved;

	int i=0;
	
	/*Create a empty List of elements*/
	ArrayList<AppointmentListViewItem> items = new ArrayList<AppointmentListViewItem>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    // Usa o SharedPreferences para  buscar o token
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
	    tokenSaved = preferences.getString("token", "");

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
    	View rootView = inflater.inflate(R.layout.listview, container,false);

        //Barra de progresso
        barraProgresso = (ProgressBar) rootView.findViewById(R.id.progressbar_loading);

        sem_marcacoes = (TextView) rootView.findViewById(R.id.semMarcacoes);


        ver_marcacoes();
    	
		return rootView;
	}

	
	// 1.1) Adding items to arrayList
    private ArrayList<AppointmentListViewItem> generateData(){

/*
        items.add(new AppointmentListViewItem("Catarina Marques", "Salao Azul", "19-08-2015", "Madeixas. (45 min)", true, false));//lista1
        items.add(new AppointmentListViewItem("Pedro Teste", "Teste", "21-08-2015", "Coloração e corte. (45 min)", false, true));//lista2
*/

		return items;
	}
    
    /*HTTP GET*/
    private void ver_marcacoes(){
    	LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){
   		
	        @Override
	        public void onTaskDone(String results) {
	            //create_list(results); // A tua funcao aqui
	        	Log.i("Teste api", results);
	        	try{

                    //Esconde a barra de progresso
                    barraProgresso.setVisibility(View.GONE);


    	        	JSONObject result= new JSONObject(results);
    	        	// Getting JSON Array node
    	        	JSONArray getmarcacoes = result.getJSONArray("getMarcaActual");
    	        	// Getting Array position 0

                    //Se não tem marcações
                    if(getmarcacoes.length()==0) {
                        sem_marcacoes.setText("Não Tem Marcações Efectuadas");
                        sem_marcacoes.setVisibility(View.VISIBLE);
                    }

    	        	for(int i = 0; i < getmarcacoes.length(); i++){

    	        		JSONObject marcacoes = getmarcacoes.getJSONObject(i);

                        String id_marcacao=marcacoes.getString("id");
    	        		String estado_marcacao=marcacoes.getString("estado_marcacao");
    	        		String nome_profissional=marcacoes.getString("nome_profissional");
        	        	String nome_estabelecimento=marcacoes.getString("nome_estabelecimento");
        	        	String data_inicio=marcacoes.getString("data_inicio");
                        String nome_servico=marcacoes.getString("nome_servico");
        	        	String duracao=marcacoes.getString("duracao");
    	        		
	    	        	Log.e("ID: "+ id_marcacao, "Profissional: " + nome_profissional + ", Servico: " + nome_servico + ", Data: " + data_inicio + ", Duracao: " + duracao);

                       //A marcação foi cancelada
                       if(estado_marcacao.equals("8")){
                           /*for(int j = 0; j < getmarcacoes.length(); j++){
                               String novo_estado_marcacao=marcacoes.getString("estado_marcacao");

                               if( novo_estado_marcacao.equals("2") ){
                                    break;
                               }
                               else if( novo_estado_marcacao.equals("7") ){
                                   break;
                               }
                               else if(novo_estado_marcacao.equals("8")){
                                   sem_marcacoes.setText("Não Tem Marcações Efectuadas");
                                   sem_marcacoes.setVisibility(View.VISIBLE);
                               }
                           }*/
                        }
                        //estado_marcacao.equals("2")-A marcação foi feita mas nao foi paga
                        else if(estado_marcacao.equals("2")){
                            items.add(new AppointmentListViewItem(id_marcacao, "Profissional: " + nome_profissional, "Salao: " + nome_estabelecimento, data_inicio, "Serviço: "+nome_servico +". ("+ duracao +"min)", true, false));//lista
                        }
                        //estado_marcacao.equals("7")-A marcação foi feita mas nao foi confirmada
                        else if(estado_marcacao.equals("7")){
                            items.add(new AppointmentListViewItem(id_marcacao, "Profissional: " + nome_profissional, "Salao: " + nome_estabelecimento, data_inicio, "Serviço: "+nome_servico +". ("+ duracao +"min)", false, true));//lista
                        }

    	        	}

                    // pass context and data to the custom adapter
    	        	AppointmentCustomListViewAdapter adapter = new AppointmentCustomListViewAdapter(getActivity(), generateData());
                    // Ordena a lista por data
                    adapter.sortByDateAsc();
                    // Binds the Adapter to the ListViews
    	        	setListAdapter(adapter);
	        	}
	        	catch(JSONException e){
	        		//Problema no parse
	        		e.printStackTrace();
					Toast.makeText(getActivity().getApplication(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
	        	}
	        }
	    };

        new RAPI(getActivity().getApplication(),"GET",FC).execute("1/marcacoes/getMarcaActual.json?token="+tokenSaved);
    }


    /**
     * Esconde o teclado
     * */
    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(getActivity().getApplication().INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }


}
