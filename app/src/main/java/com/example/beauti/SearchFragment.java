package com.example.beauti;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import android.view.inputmethod.InputMethodManager;

import com.example.beauti.LoginActivity;
import com.example.beauti.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import listview.classes.SearchCustomListViewAdapter;
import listview.classes.SearchListViewItem;
import rest.api.RAPI;


public class SearchFragment extends ListFragment implements OnClickListener {

    public SearchFragment(){}

    // Declare Variables
	ImageButton botao;
    EditText mEdit;
    private String agente="";

    ProgressBar barraProgresso;

    //Array list of professionals
    ArrayList<SearchListViewItem> arraylist = new ArrayList<SearchListViewItem>();


    Button btnLoadMore;

    // Flag for current page
    int current_page = 1;


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pesquisa_layout, container, false);

        //Barra de progresso
        barraProgresso = (ProgressBar) rootView.findViewById(R.id.progress);


        /*-----------------------------------------*/
        // Locate the EditText in pesquisa_layout.xml
        mEdit   = (EditText)rootView.findViewById(R.id.name);

        botao = (ImageButton) rootView.findViewById(R.id.procura_button);
        botao.setOnClickListener(this);


        // Getting listview from xml
        ListView lv = (ListView) rootView.findViewById(android.R.id.list);

        // Creating a button - Load More
        btnLoadMore = new Button(getActivity());
        btnLoadMore.setBackgroundColor(Color.parseColor("#3bd1c8"));//set the BackgroundColor programmatically
        btnLoadMore.setTextColor(Color.parseColor("#404040"));//set the TextColor programmatically
        btnLoadMore.setText("Ver Mais");//set the Text programmatically
        btnLoadMore.setVisibility(View.GONE);//set the visibility of the button

        // Adding button to listview at footer
        lv.addFooterView(btnLoadMore);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                barraProgresso.setVisibility(View.VISIBLE);

                current_page += 1;

                procurar_agente();
            }
        });

        //SearchCustomListViewAdapter adapter = new SearchCustomListViewAdapter(getActivity(), generateData());

        //Ordena a lista
        //adapter.sortByNameAsc();
        //adapter.notifyDataSetChanged();


        //adapter = new ListAdapter(this, arraylist);
        agente="Pedro Teste";
        procurar_agente();

        return rootView;
    }


	@Override
	public void onClick(View v) {
		int id = v.getId();

		if(id==R.id.procura_button){


            //Limpa o array antes de fazer a pesquisa
            arraylist.clear();

            agente=mEdit.getText().toString();
            //Faz a pesquisa
            procurar_agente();

        }

	}


    /**Permite procurar o agente**/
    private void procurar_agente() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);

                    int totalpages = result.getInt("totalpages_2");

                    //Mostra o botão se o nº total de páginas for maior de 1
                    if(totalpages>1){
                        btnLoadMore.setVisibility(View.VISIBLE);//set the visibility of the button
                    }
                    //Esconde o botão quando atinge o limite total de páginas
                    if(current_page==totalpages){
                        btnLoadMore.setVisibility(View.GONE);//set the visibility of the button
                    }

                    // Getting JSON array node
                    JSONArray jArr = result.getJSONArray("list");


                    for (int i=0; i < jArr.length(); i++) {


                        JSONObject obj = jArr.getJSONObject(i);
                        String agente_id = obj.getString("bea_agente_id");//Serve para mandar o id do agente para o proximo ecrã
                        String nome_agente = obj.getString("nome_agente");
                        String morada = obj.getString("morada");
                        String telefone = obj.getString("telefonesalao");

                        //Guarda o nome do agente
                        savePreferences("nome_agente", nome_agente);
                        savePreferences("morada", morada);
                        savePreferences("telefone", telefone);


                            //Vai buscar a imagem
                        JSONArray arrImage = obj.getJSONArray("imagens");
                        String imgURL = "";
                        if(arrImage.length()!=0) {
                            JSONObject objImg = arrImage.getJSONObject(0);
                            imgURL = objImg.getString("link_abs");
                         }else{
                            imgURL = "undefined.png";
                         }

                        //Esconde a barra de progresso
                        barraProgresso.setVisibility(View.GONE);

                        //Esconde o teclado após a pesquisa
                        hideSoftKeyboard();


                         //Adiciona um array custom
                         //arraylist.add(new SearchItem(prim_nome, morada, telefone));//lista
                         arraylist.add(new SearchListViewItem(agente_id, "http://www.beauti.pt/backoffice/resources/img/" + imgURL, nome_agente, telefone));//lista

                    }

                    // pass context and data to the custom adapter
                    SearchCustomListViewAdapter adapter = new SearchCustomListViewAdapter(getActivity(), generateData());
                    // Ordena a lista por ordem ascendente
                    //adapter.sortByNameAsc();
                    // Binds the Adapter to the ListView
                    setListAdapter(adapter);


                }catch(JSONException e){
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplication(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        };
        new RAPI(getActivity().getApplication(),"GET",FC).execute("1/agentes/list?nome_agente="+agente+"&orderby=4&perpage=5&page="+current_page);
    }


    /**
     * Retorna a lista
     * */
    private ArrayList<SearchListViewItem> generateData(){

        /*
        arraylist.add(new SearchListViewItem("P"));//lista
        arraylist.add(new SearchListViewItem("B"));//lista
        arraylist.add(new SearchListViewItem("A"));//lista
        arraylist.add(new SearchListViewItem("Z"));//lista
        */

        return arraylist;
    }


    /*------------------------------------------------------*/
    /* 					Save Preferences. 					*/
    /*------------------------------------------------------*/
    public void savePreferences(String key, String value) {

        //SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); // for private mode

        //Sharedpreferences object
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Permite editar Sharedpreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);// Storing string value
        editor.commit();// commit changes
    }


    /**
     * Esconde o teclado após a pesquisa
     * */
    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(getActivity().getApplication().INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

}
