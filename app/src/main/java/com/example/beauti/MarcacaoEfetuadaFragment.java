package com.example.beauti;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MarcacaoEfetuadaFragment extends Fragment implements View.OnClickListener{

    TextView servico, duracao;
    String servico_corte, servico_duracao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Alterar o action bar title
        getActivity().getActionBar().setTitle("Marcação Efetuada");

        // Usa o SharedPreferences para  buscar o token
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        servico_corte = preferences.getString("servico", "");
        servico_duracao = preferences.getString("valor_preco", "");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.marcacao_efetuada, container, false);

        servico = (TextView)rootView.findViewById(R.id.servico);
        servico.setText("Serviço: "+servico_corte);

        duracao = (TextView)rootView.findViewById(R.id.duracao);
        duracao.setText(servico_duracao);

        Button b1 = (Button) rootView.findViewById(R.id.fechar);
        b1.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id==R.id.fechar){
            //Altera de um fragmento para o outro através do FragmentManager, usando o FragmentActivity
            FragmentManager fragmentManager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.content_frame, new AppointmentFragment()).commit();

        }
    }
}
