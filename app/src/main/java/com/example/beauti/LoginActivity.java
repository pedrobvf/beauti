package com.example.beauti;


import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import rest.api.*;

import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.LoginManager;


public class LoginActivity extends FragmentActivity implements OnClickListener{

    //Actividade
	public interface FragmentCallback { public void onTaskDone(String results); }

	/**----------------------------------*/

	// Declarar Variaveis
    private LoginButton facebookLoginButton;
    private Button loginButtom, registerButton;
	private EditText emailAdress, password;
	private String txtEmail, txtPass;
	private String emailSaved, passwordSaved;
	private ProgressBar spinner;
	//Storing data of an application
	SharedPreferences preferences;
	private String tokenSaved;
    String imagem;
    //The CallbackManager, is used to manage the callbacks used in the app
    private CallbackManager callbackManager;

    public static final String TAG = LoginActivity.class.getSimpleName();
	
	
	//Call when app starts
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

        //The SDK needs to be initialized before using any of its methods
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        //initialize your instance of CallbackManager
        callbackManager = CallbackManager.Factory.create();


		// Get the view from login.xml
        setContentView(R.layout.login);


        // Usa o SharedPreferences para  buscar o token
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        tokenSaved = preferences.getString("token", "");
        
        
        //Inicializacoes
        // Locate EditTexts in login.xml (setup input fields)
        emailAdress = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        facebookLoginButton = (LoginButton)findViewById(R.id.login_button);

        final TextView info = (TextView)findViewById(R.id.info);
        loginButtom = (Button) findViewById(R.id.signin_button);
        // Locate Buttons in login.xml //setup buttons
        registerButton = (Button) findViewById(R.id.register_button);


        emailAdress.setText(preferences.getString("EMAIL", "")); //PRE-PREENCHER o EMAIL ( QUE SÒ É GUARDADO QUANDO o LOGIN deu OK, na Shared PReferences)
        password.setText(preferences.getString("PASS", "")); // Só PARA TESTES !
        /*-----------------------------------------------------*/
        //register listeners
        loginButtom.setOnClickListener(this);
        registerButton.setOnClickListener(this);

        
        //Esconde o spinner no create
        spinner = (ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
        
        /*-------------------------------------------
	     * Check if we successfully logged in before. 
	     * If we did, redirect to home page
	     *------------------------------------------*/
//         loadSavedPreferences();
//         
//         if(!emailSaved.isEmpty()&& !passwordSaved.isEmpty()){
//        	 //
//        	 launchMainActivity();
//         }

        //launchMainActivity();


        /**
         * It's time to create a callback to handle the results of the login attempts and register it with the CallbackManager
         *
         The interface has methods to handle each possible outcome of a login attempt:
         -If the login attempt is successful, onSuccess is called.
         -If the user cancels the login attempt, onCancel is called.
         -If an error occurs, onError is called.
         *
         * To register the custom callback, use the registerCallback method*/
        facebookLoginButton.registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i(TAG, "User ID: " +loginResult.getAccessToken().getUserId());
                    }

                    @Override
                    public void onCancel() {
                        Log.i(TAG, "Login attempt canceled.");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.i(TAG, "Login attempt failed.");
                    }
            }
        );


	}



	//////////////////////////////////////////////////////////////////////////////////

	//Call when user press anywhere
	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		
		if(id==R.id.signin_button){

            //launchMainActivity();

            rest_login();

		}else if(id==R.id.register_button){

            Intent intent = new Intent(this, RegistarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

		}

	}


	/*------------------------------------------------------*/
    /* 					Load Preferences. 					*/
    /*------------------------------------------------------*/
	private void loadSavedPreferences() {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		//vai buscar os dados guardados
		emailSaved = sp.getString("EMAIL", "");
		passwordSaved = sp.getString("PASS", "");
		//coloca os dados nos campos de email e password
//		emailAdress.setText(txtEmail);
		password.setText(txtPass);
	}
	
	/*------------------------------------------------------*/
    /* 					Save Preferences. 					*/
    /*------------------------------------------------------*/
	public void savePreferences(String key, String value) {

	    //Sharedpreferences object
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
	    //Permite editar Sharedpreferences
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putString(key, value);// Storing string value
	    editor.commit();// commit changes
	}
	
	
	/*------------------------------------------------------*/
    /* 					Go to MainActivity.					*/
    /*------------------------------------------------------*/
	private void launchMainActivity(){
		//Mostra a progress bar
		//spinner.setVisibility(View.VISIBLE);
        //spinner.setVisibility(View.INVISIBLE);
		
		//Aqui chama um novo intent para MainActivity, sem nunca fechar a actividade, assim posso voltar a chamar os m�todos declarados aqui
		Intent intent = new Intent(LoginActivity.this, MainActivity.class);
		startActivity(intent);

		//Fecha a actividade
		finish();
	}
	
	/**Apos guardar as tokens lanca a main activity**/
	private void rest_login(){
		txtEmail = emailAdress.getText().toString();
        txtPass = password.getText().toString();	        
        
        if (txtEmail.isEmpty() || txtPass.isEmpty()) {
        	Toast.makeText(getApplicationContext(), "Por favor preenha os campos!", Toast.LENGTH_LONG).show();
        }
        else
        {

    		/*O que fazer apos a chamada da API*/
    		   //////////Create RAPi CallBack
    	   LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){
    	        @Override
    	        public void onTaskDone(String results) {
    	            //create_list(results); // A tua funcao aqui
    	        	Log.i("Teste api", results);
    	        	try{
    	        		
	    	        	JSONObject result= new JSONObject(results);
	    	        			    	        	
	    	        	//String login_result=result.getString("login");

                        String login_result=result.get("login").toString();
	    	        	Log.i("Login JSON result",login_result);
	    	        	
//	    	        	tokenSaved
	    	        	String token=result.getString("acess_token");
	    	        	
	    	        	if(login_result.equals("1")){
	    	        		
	    	        		//grava o email e a pass nas preferencias
	    			        savePreferences("EMAIL", txtEmail);
	    			        savePreferences("PASS", txtPass);
	    	        		
	    	        		//grava a token
	    	        		savePreferences("token", token);

                            //Chama o método ver_configurações
	    	        		ver_configuracoes();
	    	        		
	    			        //lanca a mainActivity
	    			        launchMainActivity();
	    			     
	    	        	}
	    	        	else{
	    	        		Toast.makeText(getApplicationContext(), "Email ou password errados!", Toast.LENGTH_LONG).show();
	    	        	}
    	        	}
    	        	catch(JSONException e){
    	        		//Problema no parse
    	        		e.printStackTrace();
						Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
    	        	}
    	        }
    	    };
    	    
    	    new RAPI(getApplication(),"GET",FC).execute("1/users/login.json?username=" +txtEmail+ "&password=" +txtPass+ "&lang=pt-pt");
        	
        }
	}

    /** Permite ver as configuracoes do user **/
    private void ver_configuracoes() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);

                    //Utilizador
                    JSONObject utilizador = result.getJSONObject("getUserDetails");
                    String user_id = utilizador.getString("id");
                    String first_name = utilizador.getString("first_name");
                    String last_name = utilizador.getString("last_name");
                    String user_phone = utilizador.getString("telefone");

                    savePreferences("id_utiliz", user_id);
                    savePreferences("pri_nome", first_name);
                    savePreferences("ult_nome", last_name);
                    savePreferences("telefone", user_phone);

                    //Cliente
                    JSONObject cliente = result.getJSONObject("cliente");
                    String postal_cod1 = cliente.getString("codp1");
                    String postal_cod2 = cliente.getString("codp2");
                    String email = cliente.getString("email");
                    String data_nascimento = cliente.getString("data_nascimento");

                    savePreferences("cod_postal1", postal_cod1);
                    savePreferences("cod_postal2", postal_cod2);
                    savePreferences("email", email);
                    savePreferences("data_nascimento", data_nascimento);


                    //Foto
                    JSONObject fotos = result.getJSONObject("fotos");
                    //Faz um request ao diretorio da foto
                    imagem = "http://www.beauti.pt/backoffice/resources/img/";


                    if(fotos != null){
                        imagem += fotos.getString("perfil");
                    }else{
                        imagem +="undefined.png";
                    }

                    savePreferences("foto", imagem);


                }catch(JSONException e){
                    //Problema no parse
                    e.printStackTrace();
                    Toast.makeText(getApplication(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        };
        new RAPI(getApplication(),"GET",FC).execute("1/users/getuserdetails?token="+tokenSaved);
    }

	
}
