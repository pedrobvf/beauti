package com.example.beauti;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import image.ImageLoader;
import listview.classes.ProfissionalCustomListViewAdapter;
import listview.classes.ProfissionalListViewItem;
import rest.api.RAPI;

public class ProfissionalFragment extends ListFragment implements View.OnClickListener {

    public ProfissionalFragment(){}

    SharedPreferences preferences;
    String tokenSaved, IDAgente, nomeAgente, telefoneAgente, moradaAgente;
    TextView nome, telefone, morada;
    ImageView fotoPerfil;
    ListView listView;
    ProgressBar barraProgresso;
    ImageButton mapa;

    ArrayList<ProfissionalListViewItem> items = new ArrayList<ProfissionalListViewItem>();

    // key and value pair
    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

    ImageLoader imageLoader;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        //Alterar o action bar title
        getActivity().getActionBar().setTitle("Efetuar Marcação");

        //Vai buscar o ID do agente
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        tokenSaved = preferences.getString("token", "");
        IDAgente = preferences.getString("IDAgente", "");
        nomeAgente = preferences.getString("nome_agente", "");
        telefoneAgente = preferences.getString("telefone", "");
        moradaAgente = preferences.getString("morada", "");

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.listview, container,false);

        listView = (ListView) rootView.findViewById(android.R.id.list);

        //code to add header and footer to listview
        LayoutInflater inflate = LayoutInflater.from(getActivity());
        View header = inflate.inflate(R.layout.header, null);
        listView.addHeaderView(header);

        ArrayAdapter adapter = new ArrayAdapter(getActivity().getApplication(), android.R.layout.simple_list_item_1, items);

        listView.setAdapter(adapter);


        //Barra de progresso
        barraProgresso = (ProgressBar) header.findViewById(R.id.progressbar);

        //Inicializa o objecto através do método
        this.imageLoader=new ImageLoader(getActivity().getApplication());
        //barraProgresso.setVisibility(View.GONE);


        fotoPerfil = (ImageView) rootView.findViewById(R.id.foto);
        nome = (TextView) rootView.findViewById(R.id.nome);
        telefone = (TextView) rootView.findViewById(R.id.telefone);
        morada = (TextView) rootView.findViewById(R.id.morada);

        mapa = (ImageButton) rootView.findViewById(R.id.placeholder);

        ver_agente();

        mapa.setOnClickListener(this);

        return rootView;
    }

    private ArrayList<ProfissionalListViewItem> generateData(){

        return items;
    }


    /**
     * Permite procurar os detalhes da pessoa pesquisada
     * **/
    private void ver_agente() {
        LoginActivity.FragmentCallback FC= new LoginActivity.FragmentCallback(){

            @Override
            public void onTaskDone(String results) {
                try{
                    JSONObject result= new JSONObject(results);

                    // Getting JSON array node
                    JSONArray jArr = result.getJSONArray("getsingle");

                    for (int i=0; i < jArr.length(); i++) {

                        JSONObject obj = jArr.getJSONObject(i);

                        //Vai buscar a imagem
                        JSONArray arrImage = obj.getJSONArray("imagens");
                        String imgURL = "";
                        if(arrImage.length()!=0) {
                            JSONObject objImg = arrImage.getJSONObject(0);
                            imgURL = "http://www.beauti.pt/backoffice/resources/img/"+objImg.getString("link_abs");

                        }else{
                            imgURL = "http://www.beauti.pt/backoffice/resources/img/undefined.png";
                        }

                        //new ImageDownloader(imgURL, fotoPerfil).execute();
                        barraProgresso.setVisibility(View.GONE);
                        imageLoader.DisplayImage(imgURL, fotoPerfil);//Mostra a foto
                        //Mostra o nome
                        nome.setText(nomeAgente);
                        telefone.setText(telefoneAgente);
                        morada.setText(moradaAgente);
                        mapa.setImageResource(R.drawable.marker);


                        String nome_profissional = obj.getString("nome_profissional");

                        //Vai buscar o array com os estabelecimentos
                        JSONArray estab = obj.getJSONArray("estabelecimentos");
                        JSONObject objEstab = estab.getJSONObject(0);

                        String agente_id = obj.getString("bea_agente_id");
                        //Mapa
                        String nome_estabelecimento = objEstab.getString("nome_estabelecimento");
                        String latitude = objEstab.getString("latitude");
                        String longitude = objEstab.getString("longitude");

                        //Guarda os valores
                        savePreferences("agente_id", agente_id);
                        //Mapa
                        savePreferences("nome_estabelecimento", nome_estabelecimento);
                        savePreferences("latitude", latitude);
                        savePreferences("longitude", longitude);



                        JSONArray servicos= objEstab.getJSONArray("servicos");
                        for (int arr=0; arr < servicos.length(); arr++) {
                            JSONObject objServicos = servicos.getJSONObject(arr);

                            String id_servico = objServicos.getString("id_servico");
                            String nome_servico = objServicos.getString("nome");
                            String preco = objServicos.getString("preco");//Preço
                            String valor = objServicos.getString("a_partir_de");//
                            String cat_servico = objServicos.getString("valor");//Categoria do serviço


                            items.add(new ProfissionalListViewItem(id_servico, nome_servico, "Duração: "+valor +" min" + ". Por: "+ preco+ "€ "));


                            ProfissionalCustomListViewAdapter teste = new ProfissionalCustomListViewAdapter(getActivity(), generateData());
                            setListAdapter(teste);
                        }

                    }


                }catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplication(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        };

        new RAPI(getActivity().getApplication(),"GET",FC).execute("1/agentes/getsingle?lang=pt-pt&id="+IDAgente);
    }


    /**------------------------------------------------------
     ** 					Save Preferences. 					*
     **------------------------------------------------------*/
    public void savePreferences(String key, String value) {

        //SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); // for private mode

        //Sharedpreferences object
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Permite editar Sharedpreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);// Storing string value
        editor.commit();// commit changes
    }


    /**
     * Metodo que permite ir buscar fotos atraves de um url
     * */
    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String url;
        private ImageView imageView;


        public ImageDownloader(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            // If this item hasn't been recycled already, hide the
            // progress and set and show the image
            barraProgresso.setVisibility(View.GONE);

            //Mostra o nome
            nome.setText(nomeAgente);
            telefone.setText(telefoneAgente);
            morada.setText(moradaAgente);
            mapa.setImageResource(R.drawable.marker);

            //imageView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(result);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id==R.id.placeholder){

            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
    }

}
